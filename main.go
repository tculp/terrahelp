package main

import (
    "fmt"
    // "encoding/json"
    "os"
    "log"
    "context"
    // "io"
    "bytes"
    "slices"
    "flag"

	  "github.com/hashicorp/terraform-exec/tfexec"
    tfjson "github.com/hashicorp/terraform-json"

    "github.com/hairyhenderson/go-which"
)

var createResources []tfjson.ResourceChange
var updateResources []tfjson.ResourceChange
var deleteResources []tfjson.ResourceChange
var replaceResources []tfjson.ResourceChange

var nPrintf = nPrintfGen()

type parsedFlags struct {
  verbose *bool
}

func parseFlags() parsedFlags {
  var flags parsedFlags

  flags.verbose = flag.Bool("verbose", false, "Display more verbose output")

  flag.Parse()
  return flags
}

func main() {

  flags := parseFlags()

  path, err := os.Getwd()
  if err != nil {
    log.Fatalf("Error getting CWD: %s", err)
  }

  // Initialize terraform from a known binary path
  tf, err := tfexec.NewTerraform(path, findTerraform())
  if err != nil {
    log.Fatalf("Error getting terraform install: %s", err)
  }

  // Make a tempfile for the plan output to be written to
  planTempFile, err := os.CreateTemp("", "terrahelp-plan")
  if err != nil {
    log.Fatalf("Failed to generate a temp file for the plan: %s", err)
  }
  planTempFile.Close()
  defer os.Remove(planTempFile.Name())

  // Run the terraform plan, writing the output to a buffer
  outOption := tfexec.Out(planTempFile.Name())
  planOutputBuffer := new(bytes.Buffer)
  changes, err := tf.PlanJSON(context.Background(), planOutputBuffer, outOption)
  if err != nil {
    log.Fatal("Failed to run a `terraform plan`.")
  }

  if ! changes {
    fmt.Println("No changes")
    os.Exit(0)
  }

  plan, err := tf.ShowPlanFile(context.Background(), planTempFile.Name())
  if err != nil {
    log.Fatalf("Error showing terraform plan: %s", err)
  }

  totalChanges := sortResourceChanges(plan.ResourceChanges)

  // Print the changes output
  if totalChanges > 0 {
    printResourceChangeResults(*flags.verbose)
  }
  if len(plan.OutputChanges) > 0 {
    printOutputChangeResults(plan.OutputChanges, *flags.verbose)
  }
}

func sortResourceChanges(resourceChanges []*tfjson.ResourceChange) int {
  totalChanges := 0

  for _, element := range resourceChanges {
    // Ignore no-op actions
    if slices.Contains(element.Change.Actions, tfjson.ActionNoop) {
      continue
    }
    totalChanges += 1

    // Delete and Create means replace
    if slices.Contains(element.Change.Actions, tfjson.ActionDelete) {
      if slices.Contains(element.Change.Actions, tfjson.ActionCreate) {
        replaceResources = append(replaceResources, *element)
      } else {
        deleteResources = append(deleteResources, *element)
      }
      continue
    }

    // Handle Creates
    if slices.Contains(element.Change.Actions, tfjson.ActionCreate) {
      createResources = append(createResources, *element)
      continue
    }

    // Handle Updates
    if slices.Contains(element.Change.Actions, tfjson.ActionUpdate) {
      updateResources = append(updateResources, *element)
      continue
    }
  }

  return totalChanges
}

func printResourceChangeResults(verbose bool) {
  // Replaced resources
  nPrintf("Resources:\n")

  if len(replaceResources) > 0 {
    if verbose {
      nPrintf("  Replaced (%d):\n", len(replaceResources))
    }
    for _, element := range replaceResources {
      if verbose {
        fmt.Printf("    %s\n", element.Address)
      } else {
        fmt.Printf("  -/+ %s\n", element.Address)
      }
    }
  }

  // Created resources
  if len(createResources) > 0 {
    if verbose {
      nPrintf("  Created (%d):\n", len(createResources))
    }
    for _, element := range createResources {
      if verbose {
        fmt.Printf("    %s\n", element.Address)
      } else {
        fmt.Printf("  + %s\n", element.Address)
      }
    }
  }

  // Updated resources
  if len(updateResources) > 0 {
    if verbose {
      nPrintf("  Updated (%d):\n", len(updateResources))
    }
    for _, element := range updateResources {
      if verbose {
        fmt.Printf("    %s\n", element.Address)
      } else {
        fmt.Printf("  ~ %s\n", element.Address)
      }
    }
  }

  // Deleted resources
  if len(deleteResources) > 0 {
    if verbose {
      nPrintf("  Deleted (%d):\n", len(deleteResources))
    }
    for _, element := range deleteResources {
      if verbose {
        fmt.Printf("    %s\n", element.Address)
      } else {
        fmt.Printf("  - %s\n", element.Address)
      }
    }
  }
}

func printOutputChangeResults(changes map[string]*tfjson.Change, verbose bool) {
  nPrintf("Outputs (%d):\n", len(changes))
  
  for key, element := range(changes) {
    if slices.Contains(element.Actions, tfjson.ActionCreate) {
      fmt.Printf("  + %s: %s\n", key, element.After)
      continue
    }

    if slices.Contains(element.Actions, tfjson.ActionDelete) {
      fmt.Printf("  - %s: %s\n", key, element.Before)
      continue
    }

    if slices.Contains(element.Actions, tfjson.ActionUpdate) {
      fmt.Printf("  ~ %s: %s -> %s\n", key, element.Before, element.After)
      continue
    }
  }
}

func nPrintfGen() func (format string, a ...any) (n int, err error) {
  secondLine := false
  return func (format string, a ...any) (n int, err error) {
    if secondLine {
      fmt.Println()
    }
    secondLine = true

    return fmt.Printf(format, a...)
  }
}

func findTerraform() string {
  return which.Which("terraform")
}
