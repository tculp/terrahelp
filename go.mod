module gitlab.com/tculp/terrahelp

go 1.21.3

require (
	github.com/hairyhenderson/go-which v0.2.0
	github.com/hashicorp/terraform-exec v0.19.0
	github.com/hashicorp/terraform-json v0.17.1
)

require (
	github.com/apparentlymart/go-textseg/v15 v15.0.0 // indirect
	github.com/hashicorp/go-version v1.6.0 // indirect
	github.com/spf13/afero v1.3.3 // indirect
	github.com/zclconf/go-cty v1.14.0 // indirect
	golang.org/x/text v0.11.0 // indirect
)
